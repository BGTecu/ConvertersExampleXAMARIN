﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ConvertersExampleXAMARIN.Converters
{
    public class CurrencyConverter : IValueConverter
    {
        const double _Parity = 0.1;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return double.Parse(value.ToString()) / _Parity;
            }
            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return int.Parse(value.ToString()) * _Parity;
            }
            return 0;
        }
    }
}